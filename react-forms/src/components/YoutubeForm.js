import {Formik, Form, Field, ErrorMessage, FieldArray} from 'formik'
import * as Yup from 'yup'
import TextError from './TextError'
import {useState} from 'react'

const initialValues = {
    name:'',
    email:'',
    channel:'',
    comment:'',
    address:'',
    social:{
        facebook:'',
        twitter:''
    },
    phoneNumbers:['',''],
    phNumbers:['']
}

const savedValues = {
    name:'Kingshuk',
    email:'saha.kingshuk@yahoo.com',
    channel:'redmentv',
    comment:'some comment',
    address:'221B Baker Street',
    social:{
        facebook:'',
        twitter:''
    },
    phoneNumbers:['',''],
    phNumbers:['']
}

const onSubmit = (values, onSubmitProps) =>{
    console.log('form data', values)
    onSubmitProps.setSubmitting(false)
    onSubmitProps.resetForm()
}

const validationSchema = Yup.object({
    name: Yup.string().required('Required'),
    email: Yup.string().email('Invalid email format').required('Required'),
    channel: Yup.string().required('Required')
})

const validateComments = value =>{
    let error = ''
    if(!value){
        error='Required'
    }
    return error
}

function YoutubeForm() {
    const [formValues, setFormValues] = useState(null)
    return (
        <Formik initialValues = {formValues || initialValues} validationSchema={validationSchema} onSubmit={onSubmit} validateOnChange={false} enableReinitialize>
            {
                (formik)=>{
                    
                    return (
                        <Form>
                        <div className='form-control'>
                            <label htmlFor='name'>Name</label>
                            <Field type='text' id='name' name='name'/>
                            <ErrorMessage name='name' component={TextError}/>
                        </div>
                        <div className='form-control'>
                            <label htmlFor='email'>Email</label>
                            <Field type='email' id='email' name='email' />
                            <ErrorMessage name='email'>
                                {
                                    (errorMsg) => <div className='error'>{errorMsg}</div>
                                }
                            </ErrorMessage>
                        </div>
                        <div className='form-control'>
                            <label htmlFor='channel'>Channel</label>
                            <Field type='text' id='channel' name='channel' placeholder='channel name'/>
                            <ErrorMessage name='channel'/>
                        </div>
                        <div className='form-control'>
                            <label htmlFor='comment'>Comment</label>
                            <Field as='textarea' id='comment' name='comment' validate = {validateComments}/>
                            <ErrorMessage name='comment' component={TextError}/>
                        </div>
                        <div className='form-control'>
                            <label htmlFor='address'>Address</label>
                            <Field name='address'>
                                {
                                    (props)=>{
                                        const{field, form, meta} = props
                                        return <div>
                                                    <input type ='text' id = 'address' {...field}/>
                                                    {meta.touched && meta.error ? <div>{meta.error}</div>:null}
                                                </div>
                                    }
                                }
                            </Field>
                        </div>
        
                        <div className='form-control'>
                            <label htmlFor='facebook'>Facebook</label>
                            <Field type='text' id='facebook' name='social.facebook'/>
                            <ErrorMessage name='social.facebook'/>
                        </div>
                        <div className='form-control'>
                            <label htmlFor='twitter'>Twitter</label>
                            <Field type='text' id='twitter' name='social.twitter'/>
                            <ErrorMessage name='social.twitter'/>
                        </div>
        
                        <div className='form-control'>
                            <label htmlFor='primaryPh'>Primary phone number</label>
                            <Field type='text' id='primaryPh' name='phoneNumbers[0]'/>
                            <ErrorMessage name='phoneNumbers[0]'/>
                        </div>
                        <div className='form-control'>
                            <label htmlFor='secondaryPh'>Secondary phone number</label>
                            <Field type='text' id='secondaryPh' name='phoneNumbers[1]'/>
                            <ErrorMessage name='phoneNumbers[1]'/>
                        </div>
        
                        <div className='form-control'>
                            <label >List of phone numbers</label>
                            <FieldArray name='phNumbers'>
                                {
                                    (fieldArrayProps) =>{
                                        const {push, remove, form} = fieldArrayProps
                                        const {values} = form
                                        const {phNumbers} = values
                                        return <div>{phNumbers.map((phNumber,index) => (
                                            <div key={index}>
                                                <Field name={`phNumbers[${index}]`}/>
                                                {index>0?(<button type='button' onClick={()=>remove(index)}>-</button>):null}
                                                <button type='button' onClick={()=>push('')}>+</button>
                                            </div>
                                        ))}</div>
                                    }
                                }
                            </FieldArray>
                            
                        </div>
                        {/* <button type='button' onClick={()=>formik.setFieldTouched('comment')}>Visit Comments</button>
                        <button type='button' onClick={()=>formik.setTouched({name:true, email:true,channel:true, comment:true})}>Visit Fields</button>
                        
                        <button type='button' onClick={()=>formik.validateField('comment')}>Validate comments</button>
                        <button type='button' onClick={()=>formik.validateForm()}>Validate all</button> */}
                        <button type='button' onClick={()=>setFormValues(savedValues)}>Load Saved data</button>
                        <button type='reset'>Reset</button>
                        <button type='submit' disabled={!formik.isValid || formik.isSubmitting}>Submit</button>
                    </Form>
                
                    )
                }
            }
        </Formik>
    )
}

export default YoutubeForm
