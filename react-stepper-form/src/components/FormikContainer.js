import {Formik,Form} from 'formik'
import * as Yup from 'yup'
import FormikControl from './FormikControl'



function FormikContainer() {
    const dropdownOptions = [
        {key:'Select an option', value:''},
        {key:'Option 1', value:'dOption1'},
        {key:'Option 2', value:'dOption2'},
        {key:'Option 3', value:'dOption3'},

    ]

    const radioOptions = [
        {key:'Option 1', value:'rOption1'},
        {key:'Option 2', value:'rOption2'},
        {key:'Option 3', value:'rOption3'},

    ]

    const checkboxOptions = [
        {key:'Option 1', value:'cOption1'},
        {key:'Option 2', value:'cOption2'},
        {key:'Option 3', value:'cOption3'},

    ]

    const initialValues = {
        email:'',
        description:'',
        selectOption:'',
        radioOption:'',
        checkboxOption:[]
    }
    const validationSchema = Yup.object({
        email: Yup.string().email('Invalid email format').required('Required'),
        description: Yup.string().required('Required'),
        selectOption: Yup.string().required('Required'),
        radioOption: Yup.string().required('Required'),
        checkboxOption: Yup.array().min(1,'Required')
    })
    const onSubmit = values =>{
        console.log('Form data', values)
    }
    return (
        <Formik       initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}>
            {
                formik =>(
                    <Form>
                        <FormikControl control='input' type='email' label='Email' name='email'/>
                        <FormikControl control='textarea' label='Description' name='description'/>
                        <FormikControl control='select' label='Select a topic' name='selectOption' options = {dropdownOptions}/>
                        <FormikControl control='radio' label='Radio Topic' name='radioOption' options = {radioOptions}/>
                        <FormikControl control='checkbox' label='Checkbox Topics' name='checkboxOption' options = {checkboxOptions}/>

                        <button type = 'submit' disabled={!formik.isValid || formik.isSubmitting}>Submit</button>
                    </Form>
                )
            }
        </Formik>
    )
}

export default FormikContainer
