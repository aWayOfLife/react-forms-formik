import {Formik,Form, FormikConfig, FormikValues} from 'formik'
import {React, useState} from 'react'


function FormikStepper({children, ...props}) {
    console.log(children)
    const [step,setStep] = useState(0)
    const currentChild = children[step]
    const isLastStep = () =>{
        return step === children.length-1
    }
    return (
        <Formik {...props} validationSchema={currentChild.props.validationSchema} onSubmit={async(values, helpers)=>{
            if(isLastStep()){
                await props.onSubmit(values, helpers)
            }else{
                setStep(s=>s+1)
            }
        }}>
            <Form autoComplete='off'>
                {currentChild}
                {step>0 ?(<button onClick={()=> setStep(s=>s-1)}>Back</button>):null}
                <button type='submit'>{isLastStep()? 'Submit':'Next'}</button>
            </Form>
        </Formik>
    )
}

export default FormikStepper
