import {Formik,Form, FormikConfig, FormikValues} from 'formik'
import * as Yup from 'yup'
import FormikControl from './FormikControl'
import {useState} from 'react'
import FormikStepper from './FormikStepper'
import FormikStep from './FormikStep'


function ParentForm() {
    const [step,setStep] = useState(1)
    const initialValues = {
        name:'',
        dateOfBirth:'',
        phoneNumber:'',
        address:'',
        aadhar:'',
        pan:'',
        passport:'',
        rollNumber:'',
        department:'',
        CGPA:'',
    }
    const validationSchemaStepOne = Yup.object({
        name:Yup.string().required('Required'),
        dateOfBirth:Yup.string().required('Required'),
        phoneNumber:Yup.string().required('Required'),
        address:Yup.string().required('Required'),
        
    })
    const validationSchemaStepTwo = Yup.object({
        aadhar:Yup.string().required('Required'),
        pan:Yup.string().required('Required'),
        passport:Yup.string().required('Required'),
    })
    const validationSchemaStepThree = Yup.object({
        rollNumber:Yup.string().required('Required'),
        department:Yup.string().required('Required'),
        CGPA:Yup.string().required('Required'),
    })
    const onSubmit = values =>{
        console.log(values)
    }
    return (
        <FormikStepper
            initialValues={initialValues}
            onSubmit={onSubmit}
            enableReinitialize>
            
                    
                        <FormikStep validationSchema={validationSchemaStepOne}>
                            <FormikControl control='input' type='text' label='Name' name='name'/>
                            <FormikControl control='input' type='text' label='DOB' name='dateOfBirth'/>
                            <FormikControl control='input' type='text' label='Phone' name='phoneNumber'/>
                            <FormikControl control='input' type='text' label='Address' name='address'/>
                         </FormikStep>
                         <FormikStep validationSchema={validationSchemaStepTwo}>
                            <FormikControl control='input' type='text' label='Aadhar' name='aadhar'/>
                            <FormikControl control='input' type='text' label='PAN' name='pan'/>
                            <FormikControl control='input' type='text' label='Passport' name='passport'/>
                         </FormikStep>
                         <FormikStep validationSchema={validationSchemaStepThree}>
                            <FormikControl control='input' type='text' label='Roll No' name='rollNumber'/>
                            <FormikControl control='input' type='text' label='Department' name='department'/>
                            <FormikControl control='input' type='text' label='CGPA' name='CGPA'/>
                         </FormikStep>

                        {/* <button type = 'submit'>Submit</button> */}

                   
                
            
        </FormikStepper>
    )
}

export default ParentForm

