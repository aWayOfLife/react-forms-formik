function FormikStep({children, ...props}) {
    return (
        <div>
            {children}
        </div>
    )
}

export default FormikStep
