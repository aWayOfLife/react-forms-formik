
import './App.css';
import FormikContainer from './components/FormikContainer';
import ParentForm from './components/ParentForm';

function App() {
  return (
    <div className="App">
      <ParentForm/>
    </div>
  );
}

export default App;
